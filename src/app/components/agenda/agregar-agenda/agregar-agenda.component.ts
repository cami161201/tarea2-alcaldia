import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { MatSnackBar} from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { IDatos } from 'src/app/interfaces/citas';
import { CitaService } from 'src/app/servicios/cita.service';
@Component({
  selector: 'app-agregar-agenda',
  templateUrl: './agregar-agenda.component.html',
  styleUrls: ['./agregar-agenda.component.css']
})
export class AgregarAgendaComponent implements OnInit {

  form!: FormGroup;
  // PARA modificar  y cambiar
  adicionar:boolean = true;
  titulo = 'Agendar cita';


 constructor(private fb:FormBuilder,
   private _citaService:CitaService
   ,private router:Router,
   private _snackbar:MatSnackBar,
   private activateRouted:ActivatedRoute) 
   {
     this.activateRouted.params.subscribe(params =>{
       const id = params['id'];
       console.log(id);
       
       this.form=this.fb.group({
         nombre:['',[Validators.required,Validators.minLength(4),Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
         apellido:['',[Validators.required,Validators.minLength(4),Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
         correo:['',[Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
         celular:['',[Validators.required,Validators.pattern(/^([0-9])*$/)]],
         fecha:['',[Validators.required]],
         hora:['',[Validators.required]],
         descripcion:['',[Validators.required]]
       })

       if(id != 'nuevo'){
       const nombre = _citaService.buscarCita(id);
       console.log(nombre);
        if(Object.keys(nombre).length ===0){
            this.router.navigate(['/agenda']);
        }
       this.form.patchValue({
           nombre:nombre.nombre,
           apellido:nombre.apellido,
           correo:nombre.correo,
           celular:nombre.celular,
           fecha:nombre.fecha,
           hora:nombre.hora,
           descripcion:nombre.descripcion
         });
         this.adicionar = false;
         this.titulo = 'Modificar cita';
       }
      })
  }



 ngOnInit(): void {
 }

 agregarCita():void{
   if(!this.form.valid){
     return
   }

   console.log(this.form.value);

   const cita :IDatos ={
     nombre:this.form.value.nombre,
     apellido:this.form.value.apellido,
     correo:this.form.value.correo,
     celular:this.form.value.celular,
     fecha:this.form.value.fecha,
     hora:this.form.value.hora,
     descripcion:this.form.value.descripcion
   }
   console.log(cita);
   // Para modificar
   if(this.adicionar){
     this._citaService.agregarCita(cita);
     //redireccionamos a la tabla de agenda
     this.router.navigate(['/agenda']);

     this._snackbar.open('La cita fue agregada con exito','Aceptar',{
       duration:1500,
       horizontalPosition:'center',
       verticalPosition:'bottom'
     })

   } else{
     this._citaService.modificarCita(cita);
     this.router.navigate(['/agregar-agenda']);

     this._snackbar.open('La cita fue modificada con exito','Aceptar',{
       duration:1500,
       horizontalPosition:'center',
       verticalPosition:'bottom'
     });
   }
  
   
 }

 get NombreNoValido(){
   return this.form.get('nombre')?.invalid && this.form.get('nombre')?.touched;
 }
 get ApellidoNoValido(){
   return this.form.get('apellido')?.invalid && this.form.get('apellido')?.touched;
 }
 get CorreoNoValido(){
   return this.form.get('correo')?.invalid && this.form.get('correo')?.touched;
  }
  get CelularNoValido(){
   return this.form.get('celular')?.invalid && this.form.get('celular')?.touched;
 } 

  Volver(): void {
    this.router.navigate(['/agenda'])
  }
}

