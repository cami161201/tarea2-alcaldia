import { Component, OnInit,ViewChild } from '@angular/core';
import { IDatos } from 'src/app/interfaces/citas';
import { MatTableDataSource } from '@angular/material/table';
import { CitaService } from 'src/app/servicios/cita.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {

  LISTA_CITAS: IDatos[] = []

  //Para las columnas
  displayedColumns: string[] = ['nombre', 'apellido', 'correo', 'celular', 'fecha', 'hora', 'descripcion', 'acciones'];
  dataSource !: MatTableDataSource<any>;

  //Para el paginator
  // @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private _citasService: CitaService,
    private _snackbar: MatSnackBar,
    private router:Router) { }


  ngOnInit(): void {
    this.cargarCitas();
  }

  cargarCitas() {
    this.LISTA_CITAS = this._citasService.getCitas();
    this.dataSource = new MatTableDataSource(this.LISTA_CITAS)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarCita(cita:string) {

    const opcion = confirm(`¿Esta seguro de eliminar la cita de:  ${cita}?`);

    if (opcion) {
      console.log(cita);
      this._citasService.eliminarCita(cita);
      this.cargarCitas();
      this._snackbar.open("La cita fue eliminada con exito", "Aceptar", {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      })
    }
  }

  modificarCita(nombre:string){
    console.log(nombre);
    this.router.navigate(['/agregar-agenda', nombre]); 
  }

  verCita(nombre:string):void{
    console.log(nombre);
    this.router.navigate(['/ver-cita', nombre])
    
  }

}




